package smktelkom_mlg.recycleview3.model;

import java.io.Serializable;

/**
 * Created by MirzaUY on 2/14/2018.
 */

public class Hotel implements Serializable {
    public String judul, deskripsi, detail, lokasi, foto;

    public Hotel(String judul, String deskripsi, String detail, String
            lokasi, String foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.detail = detail;
        this.lokasi = lokasi;
        this.foto = foto;
    }
}
